LicenseDB
=========

This repository contains some preliminary nix expressions to build
all components of the License Database (work in progress).


Usage
-----

1.  Install the nix package manager, see: http://nixos.org/nix/
2.  Run ```nix-build``` in this folder


License
=======

Copyright 2015  Kuno Woudt

All software related to the License Database project is licensed under
the Apache License, Version 2.0. See the file Apache-2.0.txt for more
information.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied.  See the License for the specific language governing
permissions and limitations under the License.




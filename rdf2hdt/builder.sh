#!/bin/env sh

source $stdenv/setup

BUILD_DIR=`pwd`

cp -vpR "$src" src
chmod -R +w src
cd src/libcds-v1.0.12
make
cd ../hdt-lib
make tools
cd "$BUILD_DIR"

mkdir -p $out/bin
cp src/hdt-lib/tools/rdf2hdt $out/bin/
cp src/hdt-lib/tools/hdt2rdf $out/bin/

{
    binutils,
    fetchFromGitHub,
    gnumake,
    gcc48,
    overrideCC,
    stdenv
}:

let
  myenv = overrideCC stdenv gcc48;
in
  myenv.mkDerivation {
    name = "rdf2hdt-2015-09-06";
    src = fetchFromGitHub {
        owner = "rdfhdt";
        repo = "hdt-cpp";
        rev = "78a41e0c667230b5dbd40ab7f2bce998e2ad9a9e";
        sha256 = "5d8e5198193f2822b45151fdd54e868902f468f0bf4591c02d9c03b558692fbe";
    };
    builder = ./builder.sh;
}

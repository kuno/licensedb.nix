let
    pkgs = import <nixpkgs> {};
    stdenv = pkgs.stdenv;
    rdf2hdt = pkgs.callPackage ./rdf2hdt { };
in
    stdenv.mkDerivation {
      name = "licensedb-env";
      buildInputs = [ rdf2hdt pkgs.iojs pkgs.nginx ];
    }
